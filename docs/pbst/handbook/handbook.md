# 📖📚 The Handbook
::: danger ⚠ Read the handbook carefully ⚠
The following website is one of the official PBST handbooks, written by the Commanders for all PBST members to easily read and understand. (This site is programmed and developed by superstefano4/Stefano). 

**Please read it VERY carefully.**

Failure to follow the handbook may result in punishment(s), ranging from a simple warning to a demotion or even blacklist.
Any changes to this handbook will be made **clear.**
:::
## Reminders to all PBST members
  * Follow the [ROBLOX Community Rules](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules) and the Pinewood Builders Game Rules at all times. Exploiting and glitching, in particular, is a big no-no.  
    
  1. Wear your uniform and ranktag while on duty. You can only take PBST Weapons if you’re wearing these.  
    
  2. Follow orders from your superiors, and be respectful to your fellow players. Don’t act like you’re the boss of the game.  
    
  4. Use your weapons responsibly when on duty.  
  4.1. Don’t use PBST Weapons to randomly kill players or to cause a melt-or freezedown, unless TMS is hosting a reverse raid or when a Commander gives a direct order.  
  4.2. You can’t use weapons from other groups (like PET or TMS) when on duty as PBST.  
  4.3. If you use non-PBST weapons on duty (like the OP Weapons of the gamepass or credit-earned guns), you have to follow the same rules as if they were PBST Weapons.  
    
  
  5. Always give people a warning, and only engage in combat if they don’t listen.  
  5.1 This rule has exceptions, listed further down.  
      
  6. Don’t ‘revenge kill’, i.e. killing someone just because they killed you.  
  6.1 Though if they come back and start messing with core controls or breaking other rules, you may kill them without warning as they have proven themselves to be hostile (see Rule 5 exceptions).  
      
  7. Don’t restrict rooms from other players unless you know they’re hostile or intending to melt/freeze the core.
  7.1. SD+ can give an order to restrict rooms and only let on-duty PBST in.  
      
  8. Don’t KoS (Kill on Sight) anyone while on duty, unless an authorized member tells you so.  
  8.1. SD’s and above are always authorized to give KoS orders, if a TMS raid is going on ET1s will be authorized as well.
  8.2. Don’t become a mutant while on duty, mutants are always KoS.  
      
## Ranks
These are the current ranks within PBST:  
* Cadet - Starting Rank - Weak Baton
* Tier 1 - 75 Points + Evaluation - Loadout: Low Damage Baton, Shield, Taser, and Pistol.
* Tier 2 - 200 Points - Loadout: Medium Damage Baton, Shield, Taser, Pistol, and Rifle.
* Tier 3 - 400 Points - Loadout: High Damage baton, Shield, Taser, Pistol, Rifle, and Submachine Gun.
* Advanced Tier - 750 Points - Tier 3 Loadout + Gravity Disruptor.
* Elite Tier - Voted in by SDs - Advanced Tier loadout + Area Shield - First rank able to lead raid responses and host patrols
* Field Tier - Voted in by Commanders - Loadout: Elite Tier loadout + Portable Sentry
* Special Defense - 950 Points + Voted by Commanders + SD Evaluation - Loadout: Field Tier loadout + Katana, **Upgraded** Rifle, Pistol, and Submachine Gun - First rank to host Trainings
* Commanders - Be an SD + Voted in by ALL Commanders + HOS
* Head of Security - Cannot be obtained, given by Diddleshot - The Head of PBST 
* Owner - Unobtainable - The Chairman of Pinewood. (The man Diddleshot himself)

These ranks are currently within PBST, [more information about the ranks can be found here](supporting-files/ranks.md).

## Going on duty to patrol
Before you can do your job as Pinewood Security, you need to show to civilians and other PBST that you are on duty as Security. To do that, you have to wear a uniform and have your Kronos ranktag be set to Security. Having only one of these will not make you count as on-duty, and you won’t be allowed to take PBST Weapons.

This image is made by the **Commander [EquilibriumCurse](https://www.roblox.com/users/962201589/profile)**.  
It shows all the requirements you need to have to be ``On Duty``!
![img](/PBST-On-Duty.png)

### Uniform 
The official PBST uniforms can be found in the [store of the PBST group](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/store).
All facilities have a shopping cart button on your screen where you can buy these uniforms as well. 
Buying a uniform from one of these methods and equipping your Roblox avatar with it is the most convenient way to get an official unifrom.

If you have no robux, most facilities are equipped with uniform givers to get you going. 
It’s also possible to buy donor commands and use the ```!shirt``` & ```!pants``` commands that come with that to get an official uniform.

#### Uniform Giver locations:
* [Pinewood Computer Core:](https://www.roblox.com/games/17541193/Pinewood-Computer-Core)  
  You can access it from the main spawn through the lobby, up the elevator (follow ‘Security Sector + Cafe)’, in the PBST room to the right.
* [Pinewood Research Facility:](https://www.roblox.com/games/7692456/Pinewood-Research-Facility)  
  You can access it from the main spawn into the room marked ‘PB SEC’, to the hallway between the baton giver and the coffee machine, changing room on the left.
* [Pinewood Builders HQ:](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ)  
  You can access it on 4th floor, from elevators to the glass doors on the left, blue doors on the left.
* [PBST Training Facility:](https://www.roblox.com/games/298521066/PBST-Training-Facility)  
  You can access it at the right side of the spawn building.
* [PBST Activity Center:](https://www.roblox.com/games/1564828419/PBST-Activity-Center)  
  You can access it into the main hallway of the lobby.  
 ![img](/pbstac_uniform.jpeg)


[Pinewood Space Station](https://www.roblox.com/games/17541177/Pinewood-Space-Station), [Pinewood Space Shuttle Advantage](https://www.roblox.com/games/17541196/Pinewood-Space-Shuttle-Advantage) and [Mega Miners](https://www.roblox.com/games/17541179/Mega-Miners).

If you need to get a Hazmat suit to enter the core, use the command `!pbsthazmat` after equipping the suit to get a PBST lanyard. The Hazmat suits are not official PBST uniforms, you are only allowed to wear them to enter PBCC’s core if you have the lanyard and keep your ranktag on Security.
The Medical Suit is not allowed because unlike the other two, this one actually changes your shirt & pants, making it impossible to wear a PBST uniform with it.

![img](/pet_uniform.jpeg)

![img](/FS_Update.png)

## Ranktag 
The ranktag is the small text above your avatar in Pinewood facilities. This will be set to Security by default, displaying your rank in PBST.  
![img](/ranktag_pbst.png)  

### Supplementary ranktag roles
On the PBST Communication Platform, there are several roles that people can earn which will be displayed on their ranktag. These are:  
 * Member of The Season, a trimonthly given role to a single outstanding member of PBST
 * PBST Medals, annually awarded roles for top performances in various departments

If your ranktag is off, you can use the command **``!ranktag on``** to turn your ranktag on. If it’s set to another group than Security, use **``!setgroup PBST``** to get it set to Security so you can go patrol.

Off-duty PBST may still have their ranktag on, because they forgot to turn it off or are just new and don’t know how it works. ***This is NOT against the handbook, they are simply considered off-duty.***

Once you have your uniform and ranktag, you are on duty and you can take PBST Weapons from the loadouts. When on duty, it is your job to protect the facility from various types of threats that may arise, like meltdowns, raiders and OP Weapon users. When you have to evacuate, try your hardest to help others.

### Going off duty
To go off-duty, you have to visit the Security room and remove your uniform and PBST weapons. Reset character if required. It is highly recommended you remove your Security ranktag as well, by using the command `!setgroup PB/PET/PBQA...` to change to any other Pinewood group you’re in, or use the `!ranktag off` command to turn the ranktag off entirely.

## Usage of weapons
All PBST are expected to use their weapons responsibly when on duty. If you find somebody breaking a rule or doing anything to contribute to a melt- or freezedown, you have to give this person a warning. If he/she doesn’t listen to your warning, you are allowed to kill them.

::: danger Weapon usage in Safe-Zones
Attacking inside safe-zones (Spawn and PET Emergency Rooms) is not allowed unless you are already in combat with them and they ran inside said locations to avoid getting killed.
:::


There are a few occasions where you can kill players without needing to warn them first:


 * People who have been warned and killed for changing the core controls to melt- or freezedown, and still come back to do the same
 * On-duty TMS members
 * Off-duty TMS members but **with their ranktag on and are messing with the controls to cause a meltdown or a freezedown**.
 * People with OP Weapons or otherwise acquired gear who are attacking Security or visitors
 * People who are spamming the reactor power button at PBCC, trying to get it locked
 * People who are caught sabotaging the coolant pipe (or fixing it if the core is approaching freezedown)
 * During melt- or freezedown only, people who touch the emergency coolant and rockets at PBCC
 * However, you cannot ‘revenge kill’, i.e. killing someone just because they killed you. Though if they come back and start messing with the controls or breaking other rules, you may kill them without warning as they have proven themselves to be hostile.

[PBST have access to a wide variety of weapons to protect the games with. More information about the weapons can be found on this page.](supporting-files/usage-of-weapons.md)

## Important facilities to patrol
Pinewood has 2 important facilities to patrol, these are:  
 - [Pinewood Builders Computer Core](supporting-files/important-facilities-to-patrol.md#pinewood-builders-computer-core)
 - [PBST Activity Center (PBSTAC)](supporting-files/important-facilities-to-patrol.md#pbst-activity-center-pbstac)

## Other groups you may encounter
 - [Pinewood Emergency Team (PET)](supporting-files/other-groups-you-may-encounter.md#pinewood-emergency-team-pet)
 - [The Mayhem Syndicate (TMS)](supporting-files/other-groups-you-may-encounter.md#the-mayhem-syndicate-tms)

## Trainings and ranking up
**NOTE: Instructions in trainings are given in English, so understanding English is required.**  
You need to earn PBST Points to rank up in the group. The requirements for any rank can be found at the spawn in [PBST Activity Center](https://www.roblox.com/games/1564828419/PBST-Activity-Center). **Note that ST Exp and Patrol Time of the Activity Center are not the same as PBST points, nor are PBCC Credits.**

You can check how many points you have using the ``!mypoints`` command at any Pinewood facility that has PB Kronos ingame (Kronos with a Sythe), or in the Points Room of the [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility).

Points can be earned through TMS Raids, trainings, mass patrols, and self-training, [information about these events can be found here.](supporting-files/trainings-and-ranking-up.md)

### Commonly used abbreviations
#### Training Types
DT - Disciplinary Training   
MT - Mega Training  
HT - Hardcore Training  
ST - Self Training **(Tier 1+ only)**

#### Training Commands
STS - Shoulder to Shoulder   
SFL - Single File Line  
DFL - Double File Line  
TFL - Triple File Line  
PTS - Permission To Speak  

#### Facility Acronyms
PBCC - Pinewood Builders Computer Core  
PBRF - Pinewood Builders Research Facility  
PBHQ - Pinewood Builders HeadQuarters  
PBSSA - Pinewood Space Shuttle Advantage  
PBSTAC - Pinewood Builders Security Team Activity Center  
PBSTTF - Pinewood Builders Security Team Training Facility  
PBDSF - Pinewood Builders Data Storage Facility  
PBSTH - Pinewood Builders Security Team Hub  
PBOP - Pinewood Builders Oil Platform  
MM - Mega Miners  
  
#### Group Acronyms
PBST - Pinewood Builders Security Team  
TMS - The Mayhem Syndicate  
PET - Pinewood Emergency Team  
PBM - Pinewood Builders Media  
PBQA - Pinewood Builders Quality Assurance  
PBA - Pinewood Builders Aerospace  
PIA - Pinewood Intelligence Agency  
PBV - Pinewood Builders Veterans    

#### Ranks
T# - Tier #  
AT - Advanced Tier
ET - Elite Tier 
FT - Field Tier
SD - Special Defense  

#### Misc
KoS - Kill on Sight  
S# - Sector # 
