# ✊ Punishments
Within PBST there are a few punishments that you can get. These differ from a point deduction, to a full on trelloban.

## PBST Punishments
There are a few PBST Specific punishments that you can get. One of these is the nicest one of these, or better saying the most lenient.
- Point Deductions
- Demotion
- Blacklist
- Ranklock

### Point Deductions
If you violated one of the handbook rules, or you've acted not like your rank. Either a Trainer or SD can punish you. The most basic one is a point deduction. For this, there are 2 versions of this. 

#### **Version 1 (Special Defense Deduction)**:   
SD's can on their own accord, deduct 20 points without trainer supervision of your account. (Keep in mind, trainers still have to log the points. So Trainers can always invalidate the minus points if they do not seem the deduction fit). However, in ***most*** cases there are valid reasons for the deduction. 

If SD's want to deduct more then 20 points, they can request a heavier deduction with the trainers. This can be anything from a -20, to a -500 (This has already happened once). 

#### **Version 2 (Trainer Deductions)**:  
Trainer Deductions can happen much easier then Special Defense Deductions. Instead of having to ask or wait for a deduction, trainers can instantly run the deduction. This will mostly depend on how bad you did something. But the general condition is it being applied instantly. 

Trainers can deduct any amount of points, and it can be instantly logged into the database, so no waiting on the points to be logged.

### Demotion
There are 2 ways you can get demoted. This will happen on the following ranks:  
    - Tier 1, 2 & 3   
    - Elite Tier 1 & 2  

#### **Version 1 (Points Based Demotion)**:   
If you've lost enough points to go below the required points of a rank, you might be demoted 1 rank lower then you currently are. This means you can be demoted back to the rank below the point requirement. This means you'll be required to regain all points you lost to get back to the required point amount. 

You might have to redo the tier evaluation you get demoted from T1 to Cadet. This will depend on your behavior with point deductions. See the next version for more information.

#### **Version 2 (Punishment Requested Demotion)**:  
If  you're a Tier 1 to ET 2, but you're not acting like it, then you might have a chance to get requested to be demoted from your current rank, to a lower one. If the requester thinks you did a worse job, you'll have your evaluation status reset back to a cleared profile. Meaning you have to redo your entire evaluation. Otherwise you will not be able to regain your rank when you when you get the points required for it.

If you're a Pre-Tier (Tier before the evaluations where set into place), and you still have the points (But you lost the rank, maybe because you left or because you got demoted) you can do the evaluation, and request a trainer not to reset your points There will be asked for proof if you've actually gotten the rank before. 

### Ranklock
An ranklock is straight forward, your locked on the rank you where placed on. If you have a ranklock as a Cadet, you'll be unable to rank up to T1, or join the evaluation server. Untill this ranklock is removed, you wont be able to attend. If you have a ranklock as a Tier 1 or above. You will not be able to rank up in PBST, even though you can still earn point. Your rank will not be changed for the entire duration of the ranklock.

### Blacklist
Blacklists can be requested by any rank, but only Trainer+ can run them. These are restrictions for the people who violate the rules to such extend, that they're not allowed in PBST Events, because they manage to mess up the game so badly, that they ruin the event for everyone else. 

Blacklists will keep you out of official PBST places like PBSTAC and PBST Hub. This blacklist will pertain during trainings, and will be ran every time a training starts. 

## PB Punishments
Pinewood Builders is the main group where everything is ran from, with this. There will be some punishments that parent of the main group
- Globalban *(Discord, managed by PIA+)*
- Trelloban *(In-game, managed by Facilitator+)*

### Globalban (Discord)
If you've gained a global ban on the Pinewood Discord groups, you are banned from all discord groups. This means you can't talk to others on any of these servers. The only server you'll be able to is the [Pinewood Builders Appeals Center (PBAC)](https://discord.gg/mWnQm25) (And this is your only place where you can request an unban). 

These global bans will also be ran when you've been placed on a Trelloban, an explanation about that below.

### Trelloban (In-Game)
Trellobans are global bans from everything connected to Pinewood, the only way to get removed from a Trelloban is appealing with a Facilitator. This can be done through the PBAC.